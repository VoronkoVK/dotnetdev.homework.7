﻿using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace WebClient
{
    static class Program
    {
        const string BaseUrl = "http://localhost:5000/";

        static async Task Main(string[] args)
        {
            var createdCustomerId = await CreateCustomer();
            await GetCustomerById(createdCustomerId);
        }

        private static async Task<long> CreateCustomer()
        {
            var customer = RandomCustomer();

            var client = new HttpClient();
            var jsonContent = new StringContent(
                JsonSerializer.Serialize(customer),
                Encoding.UTF8,
                "application/json"
            );
            var response = await client.PostAsync(BaseUrl + "customers", jsonContent);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"Can't create customer. Status code: {response.StatusCode}");
            }
            
            var id = long.Parse(await response.Content.ReadAsStringAsync());
            return id;
        }

        private static async Task GetCustomerById(long id)
        {
            var client = new HttpClient();
            var response = await client.GetAsync(BaseUrl + $"customers/{id}");
            
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"Can't get customer by Id({id}). Status code: {response.StatusCode}");
            }

            var customerJson = await response.Content.ReadAsStringAsync();
            Console.WriteLine(customerJson);
        }

        private static CustomerCreateRequest RandomCustomer()
        {
            var firstNames = new string[] { "Ivan", "Roman", "Oleg", "Petr", "Alexandr" };
            var lastNames = new string[] { "Ivanov", "Romanov", "Petrov", "Alexandrov" };

            var customer = new CustomerCreateRequest()
            {
                Firstname = firstNames[new Random().Next(firstNames.Length)],
                Lastname = lastNames[new Random().Next(lastNames.Length)]
            };

            return customer;
        }
    }
}
