using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
using WebApi.Storages;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        private readonly ICustomerStorage _storage;

        public CustomerController(ICustomerStorage storage)
        {
            _storage = storage;
        }

        [HttpGet("{id:long}")]   
        public ActionResult<Customer> GetCustomerAsync([FromRoute] long id)
        {
            var customer = _storage.GetCustomerByID(id);

            if (customer is null)
            {
                return NotFound();
            }

            return customer;
        }

        [HttpPost("")]   
        public ActionResult<long> CreateCustomerAsync([FromBody] Customer customer)
        {
            var newCustomer = _storage.CreateCustomer(customer.Firstname, customer.Lastname);
            
            return newCustomer.Id;
        }
    }
}
