﻿using System.Collections.Generic;
using System.Linq;
using WebApi.Models;

namespace WebApi.Storages
{
    public class CustomerStorage : ICustomerStorage
    {
        private long _idCounter;
        private List<Customer> _customers = new();

        public Customer GetCustomerByID(long id)
        {
            return _customers.FirstOrDefault(c => c.Id == id);
        }

        public Customer CreateCustomer(string firstName, string lastName)
        {
            _idCounter++;

            var customer = new Customer()
            {
                Id = _idCounter,
                Firstname = firstName,
                Lastname = lastName
            };

            _customers.Add(customer);

            return customer;
        }
    }
}
