﻿using WebApi.Models;

namespace WebApi.Storages
{
    public interface ICustomerStorage
    {
        Customer GetCustomerByID(long id);
        Customer CreateCustomer(string firstName, string lastName);
    }
}